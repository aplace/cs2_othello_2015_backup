CC          = g++
CFLAGS      = -Wall -ansi -pedantic -ggdb -std=c++0x
OBJS        = player.o board.o
PLAYERNAME  = fbap

all: $(PLAYERNAME) testgame parameter_test_control
	
$(PLAYERNAME): $(OBJS) wrapper.o
	$(CC) -o $@ $^

testgame: testgame.o
	$(CC) -o $@ $^

testminimax: $(OBJS) testminimax.o
	$(CC) -o $@ $^

parameter_test_control: $(OBJS) parameter_test_control.o
	$(CC) -o $@ $^

%.o: %.cpp
	$(CC) -c $(CFLAGS) -x c++ $< -o $@
	
java:
	make -C java/

cleanjava:
	make -C java/ clean

clean:
	rm -f *.o $(PLAYERNAME) testgame testminimax
	
.PHONY: java testminimax
