#include <string>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <vector>

#define GAME_CALL_COMMAND "./testgame player BetterPlayer &"
#define GAME_KILL_COMMAND "killall java &"

#define INPUT_FILE_NAME "params.txt" // refer to input and output of board.cpp, not this program
#define GAME_STATUS_FILE_NAME "status.txt"
#define OUTPUT_FILE_NAME "result.txt"

#define PARAM_TO_MAXIMIZE cornerWeight

#define WAIT_TIME 1000
// in microseconds

void write_params(std::vector<int> inparam)
{
	std::ofstream paramFile;
	paramFile.open(INPUT_FILE_NAME, std::ios::trunc);
	if (paramFile.is_open())
	{
		for (std::vector<int>::iterator it = inparam.begin(); it != inparam.end(); it++)
		{
			paramFile << *it << std::endl;
		}
	}
	paramFile.close();
	return;
}

// waits for game to finish and returns status; 0 for loss, 1 for win
int get_result()
{
	while (1)
	{
		std::ifstream resFile;
		resFile.open(GAME_STATUS_FILE_NAME);
		if (resFile.is_open())
		{
			int res;
			resFile >> res; // 0 for game still in progress, 1 for complete
			if (res)
			{
				resFile >> res; // read the result of the game and return it
				std::cerr << "parameter test control result: " << res << std::endl;
				std::system(GAME_KILL_COMMAND);

				return res;
			}
			usleep(WAIT_TIME);
		}
	}
}

/* parameter order in inParam:
		file >> cornerWeight;
		file >> diagonalToCornerWeight;
		file >> inlineWithCornerWeight;
		file >> row2Weight;
		file >> row3Weight;
		file >> row4Weight;
		file >> edgeWeight;
		file >> permPosWeight;
		file >> openMoveWeight;
*/
int maximize_one_parameter(std::vector<int> inParam, int loc, int stepSize, int numTrials, int numTests)
{	
	int val = 0;
	int res;
	int sequenceRes;
//	int loosingSequence = 0;

	std::ofstream resFile;
	resFile.open(OUTPUT_FILE_NAME, std::ios::app);
	for (int i = 1; i <= numTrials; i++)
	{	
		sequenceRes = 0;
		resFile << "Changing parameter number: " << loc << " to value " << val << std::endl;

		inParam[loc] = val;
		write_params(inParam);
		for (int j = 0; j < numTests; j++)
		{

			std::ofstream statFile;
			statFile.open(GAME_STATUS_FILE_NAME, std::ios::trunc);
			statFile << 0;
			statFile.close();
			std::system(GAME_CALL_COMMAND);
			res = get_result(); // 1 for win, 0 for loss
			if (res)
			{
				sequenceRes ++;
			}
		}
	}
	//resFile.close();
	std::stringstream catRes;
	catRes << "cat " << OUTPUT_FILE_NAME;
	std::system(catRes.str().c_str());
	return 0;
}

int main()
{

	int paramList[] = {0, -10, 0, 0, 0, 0, 40, 100, 0}; // enter params in order as given in setParams in board.cpp
	std::vector<int> paramVector (paramList, paramList + sizeof(paramList) / sizeof(int));
	maximize_one_parameter(paramVector, 0, 1, 1, 1);
}
