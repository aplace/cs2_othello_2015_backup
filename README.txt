What each group member did:
Fabian:
	Alpha Beta Pruning
	Got bot working -> random player
	wrote minimax

Alex:
	wrote heuristic
		- parameter test system (see parameter_test_control_file)
		- parameter helper functions
		- actual heuristic function
	wrote memoization (not used in final push)
	a lot of debugging


Changes we made to make tournament worthy:
	advanced heuristic--based on the permanent positions the player holds
	memoization--not used because introduced too many bugs at last minute (this iteration included iterative deepening, and we were going to include an opening table but we couldn't find one online)
	bitmap in heuristic to increase speed
	parameter test system to optimize parameters

why our strategy will work:
	The heuristic is what we believe to be the most crucial part of the system because even if you have a lot of depth in your search, if you don't have a good heuristic to evaluate the result your depth has gone to waste.

