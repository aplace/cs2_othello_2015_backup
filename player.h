#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#define TOTAL_MS 16 * 60 * 1000

#define DEPTH 8

using namespace std;

enum Corner
{
	TOPLEFT, TOPRIGHT, BOTTOMLEFT, BOTTOMRIGHT
};

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    int updateParams(); // read params in from a file

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    int depth;
    Board* board;
    Side color;
};

#endif
